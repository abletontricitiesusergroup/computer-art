# CSOUND AND CSOUND VST
Version 6.03.2

[![Build Status](https://travis-ci.org/csound/csound.svg?branch=develop)](https://travis-ci.org/csound/csound)

A user-programmable and user-extensible sound processing language
and software synthesizer.

Csound is copyright (c) 1991 Barry Vercoe, John ffitch.
CsoundVST is copyright (c) 2001 by Michael Gogins.
VST PlugIn Interface Technology by Steinberg Soft- und Hardware GmbH

Csound and CsoundVST are free software; you can redistribute them
and/or modify them under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

Csound and CsoundVST are distributed in the hope that they will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this software; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
02111-1307 USA

# GETTING STARTED

CsoundQt, a graphical shell for Csound, makes an excellent place to begin 
the exploration of Csound, especially because CsoundQt has an extensive menu 
of built-in examples that show off Csound's capabilities. Many musicians make 
CsoundQt their main Csound environment, although there are other excellent 
environments. CsoundQt runs not only Csound code, but also Python scripts.

CsoundVST is a VST plugin version of Csound. Please see "A Csound Tutorial"
herein for instructions on use. CsoundVST does not support custom VST GUIs,
but allows the Csound orchestra to be edited and saved in the VST host.

The tutorial/tutorial.pdf file is an illustrated tutorial of how to 
find, install, configure, and use Csound. To get started with 
real-time MIDI performance, for example, see Section 2.1.3.

The tutorial/Csound_Algorithmic_Composition_Tutorial.pdf is an illutrated
tutorial of how to use CsoundAC to do algorithmic composition (score
generation). This tutorial includes several complete pieces.

The examples directory contains numerous working Csound orchestras and 
even complete pieces. The examples/Boulanger_Examples directory contains
hundreds of working Csound orchestras.

The doc/manual/html/indexframes.html file is the front page to the 
Csound Reference Manual.

This version of Csound is programmable in Python, Java, Lua, and LISP, 
and scores can be generated in these languages. 

The doc/manual/api/index.html file is the front page to the C/C++ 
Csound and Csound API application programming interfaces reference,
but it is also helpful when programming Csound in other languages.

# CONTRIBUTORS

Csound contains contributions from musicians, scientists, and programmers
from around the world. They include (but are not limited to):

* Allan Lee
* Bill Gardner
* Bill Verplank
* Dan Ellis
* David Macintyre
* Eli Breder
* Gabriel Maldonado
* Greg Sullivan
* Hans Mikelson
* Istvan Varga
* Jean Piché
* John ffitch
* John Ramsdell
* Marc Resibois
* Mark Dolson
* Matt Ingalls
* Max Mathews
* Michael Casey
* Michael Clark
* Michael Gogins
* Mike Berry
* Paris Smaragdis
* Perry Cook
* Peter Neubäcker
* Peter Nix
* Rasmus Ekman
* Richard Dobson
* Richard Karpen
* Rob Shaw
* Robin Whittle
* Sean Costello
* Steven Yi
* Tom Erbe
* Victor Lazzarini
* Ville Pulkki
* Andres Cabrera
* Felipe Sataler
* Ian McCurdy
