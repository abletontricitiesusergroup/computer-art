#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include <stdexcept>

namespace tl
{
	namespace antlr4
	{



		class Main
		{
			static void main(std::vector<std::wstring> &args);
		};

	}
}
